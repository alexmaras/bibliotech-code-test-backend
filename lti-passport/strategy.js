
// get the basic passport and LTI stuff required
var passport = require('passport-strategy');
var util = require('util');
var lti = require('ims-lti');
// a useful function for checking if properties exist on objects
var lookup = require('./utils').lookup;

// Import the models. 
//
// Honestly, I'm not happy that this is here. 
// The passport authentication shouldn't have to know about
// the application structure - this is no longer portable. 
//
// However, I needed a way to identify the LTIConsumer when _initialising_
// the Stratery, and I'm not sure how I can do that. I will probably 
// try to figure this out later. Essentially, I needed to be able
// to match the consumerKey to the one in the database, and that
// can only happen once I've got the request. 
const models = require('../models/index.js')('sqlite3.db');

// Define the passport strategy
function Strategy(options, verify) {

  // ignore options if it's not passed
  if (typeof options == 'function') {
    verify = options;
    options = {};
  }

  if (!verify) { throw new TypeError('LTIStrategy requires a verify callback'); }

  passport.Strategy.call(this, options, verify);
  // sets the strategy name that will be called in middleware
  this.name = 'lti';
  this._verify = verify;
}

util.inherits(Strategy, passport.Strategy);

// The authenticate call that gets called when hitting the /login equivalent route
Strategy.prototype.authenticate = function(req, options) {

  // ensure there is at least a consumerKey and a consumerSecret, otherwise
  // the request is borked
  var consumerKey = lookup(req.body, 'oauth_consumer_key');
  var signature = lookup(req.body, 'oauth_signature');

  if(!consumerKey || !signature) {
    return this.fail({ message: 'Missing credentials' }, 400);
  }

  var self = this;

  var consumerKey = req.body.oauth_consumer_key;

  // Get the LTIConsumer (see my comment up top about this)
  // and also include the institution to get the institution name. 
  models.LTIConsumer.findOne({
    where: { oauth_consumer_key: consumerKey },
    include: [{
      model: models.Institution
    }]
  }).then(ltiConsumer => {

    // if we don't have an ltiConsumer, then the consumerKey didn't exist
    // in the database
    if(!ltiConsumer) {
      return self.fail({ message: 'Missing credentials' }, 401);
    }

    // use the ims-lti package and feed in the key and secret
    provider = new lti.Provider(ltiConsumer.oauth_consumer_key, ltiConsumer.oauth_consumer_secret);

    // then validate the request
    provider.valid_request(req, (err, isValid) => {

      if(isValid) {

        // this is the callback that the _verify callback with call back to when it's done()
        function verified(err, user, info) {
          if (err) { return self.error(err); }
          if (!user) { return self.fail(info); }
          self.success(user, info);
        }

        try {
          // we're happy that this is a valid oauth1 request, we can shoot off to verify to fetch the user
          this._verify(req, provider.body.lis_person_contact_email_primary, provider.body.lis_person_name_full, ltiConsumer.Institution, provider, verified);
        } catch (ex) {
          return self.error(ex);
        }
      } else {
        return self.error(err);
      }
    });
  }
  );

};



module.exports = Strategy;
