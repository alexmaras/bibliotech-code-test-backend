const express = require('express');
const app = express();
var session = require('express-session');
const bodyParser = require('body-parser');

var passport = require('passport');
var LTIStrategy = require('./lti-passport');
const models = require('./models/index.js')('sqlite3.db');

// Defining how to serialize and deserialize users for use in sessions
passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  models.User.findByPk(id)
    .then(user => {
      return done(null, user);
    });
});

// The callback for creating or fetching the user
passport.use(new LTIStrategy(
  function(req, email, name, institution, provider, done) {
    // Find ot create the user with all the necessary data
    models.User.findOrCreate({
      where: {
        name: provider.body.lis_person_name_full,
        email: provider.body.lis_person_contact_email_primary,
        is_student: provider.student,
        is_academic: provider.alumni || provider.instructor, // Arbritrary choice of roles based on limited knowledge
        InstitutionName: institution.name,
      }
    }).then(function(user, created) {
      if(!user) {
        return done(null, false, { message: 'Something went wrong' });
      }
      // and return the user
      return done(null, user[0]);
    })
  })
);

// a tiny middleware to check if the request is authenticated by Passport's sessions
function checkAuthentication(req,res,next){
  if(req.isAuthenticated()){
    next();
  } else{
    res.status(401).send("Unauthorised");
  }
}

const index = require('./routes/index.js');
const usersLTI = require('./routes/users/lti.js');
const books = require('./routes/books.js');
const bookAccessible = require('./routes/bookaccessible.js');

function stub(req, res) {
  return res.send(`${req.url} not implemented`);
}

app.use(session({ secret: "bibliotech-code-test" }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(passport.initialize());
app.use(passport.session());

app.get('/', index)
app.get('/books', checkAuthentication, books)
app.get('/books/:bookId(\\d{13})', checkAuthentication, bookAccessible)
app.post('/users/lti', passport.authenticate('lti'), usersLTI);

module.exports = app;
