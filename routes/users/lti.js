/**
 * The LTI authentication route handler.
 * Responds to a request for authentication.
 */

module.exports = (req, res, next) => {
  // Doesn't have to do anything - could output a messsage or redirect with 
  // a more realistic web application
  console.log('Authenticated');
  res.json({ message: "Successfully authenticated" });
}
