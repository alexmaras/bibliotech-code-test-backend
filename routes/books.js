/**
 * The books route handler.
 * Responds to a request for a list of available books with a JSON structure.
 */

const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const models = require('../models/index.js')('sqlite3.db');

module.exports = (req, res, next) => {

  // Not happy with this solution - 
  // this was due to a bad structuring decision in the beginning. 
  // A better option would have been to have a Roles table and 
  // defined some allowed roles for users to have access to books. 
  var whereClause = [];
  if(req.user.is_student) {
    whereClause.push({ student_accessible: true });
  }
  if(req.user.is_academic) {
    whereClause.push({ academic_accessible: true });
  }

  // Gets the institution that the user belongs to
  req.user.getInstitution().then(institution => {
    // gets all allowed books
    institution.getAllowedBooks({
      through: {
        // where the user has the appropriate role
        where: { [Op.or]: whereClause }
      }
    }).then(books => {
      // and returns the books as JSON
      res.json(books);
    });

  });
}
