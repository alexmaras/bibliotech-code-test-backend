/**
 * The books route handler.
 * Responds to a request for a list of available books with a JSON structure.
 */

const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const models = require('../models/index.js')('sqlite3.db');

module.exports = (req, res, next) => {

  // see books.js
  var whereClause = [];
  if(req.user.is_student) {
    whereClause.push({ student_accessible: true });
  }
  if(req.user.is_academic) {
    whereClause.push({ academic_accessible: true });
  }

  req.user.getInstitution().then(institution => {
    institution.getAllowedBooks({
      through: {
        where: { [Op.or]: whereClause }
      }
    }).then(books => {
      // The only different between this and books 
      var requestedBook = books.find(function(book) {
        // if the ISBN matches the requested ISBN
        return req.params.bookId == book.isbn;
      });
      if(requestedBook) {
        res.json({ message: 'This book is available to you' });
      } else {
        res.json({ message: 'This book is not available to you' });
      }
    });

  });
}
