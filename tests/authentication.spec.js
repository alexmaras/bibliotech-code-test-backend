const supertest = require('supertest');
const asset = require('assert');
const app = require('../index');

describe("LTI Authentication", function() {

  describe("Without Authentication", function() {

    describe("GET /books returns 401", function() {
      it("returns 401", function(done) {
        supertest(app)
          .get("/books")
          .expect(401)
          .end(function(err, res){
            if (err) { done(err); } 
            else { done(); }
          });
      });
    });

    describe("GET /books/1234567890123 returns 401", function() {
      it("returns 401", function(done) {
        supertest(app)
          .get("/books/1234567890123")
          .expect(401)
          .end(function(err, res){
            if (err) { done(err); } 
            else { done(); }
          });
      });
    });

    describe("POST /users/lti without auth body returns 400", function() {
      it("returns 400", function(done) {
        supertest(app)
          .post("/users/lti")
          .expect(400)
          .end(function(err, res){
            if (err) { done(err); } 
            else { done(); }
          });
      });
    });


  });

  var login = require('./login');
  describe("Handling Authentication", function() {
    describe("POST /users/lti without auth body returns 400", function() {
      it("returns 400", function(done) {
        supertest(app)
          .post("/users/lti", {

          })
          .expect(400)
          .end(function(err, res){
            if (err) { done(err); } 
            else { done(); }
          });
      });
    });
  });

});
