const supertest = require('supertest');
const asset = require('assert');
const app = require('../index');

describe("Basic Server Functionality", function() {
  describe("GET /", function() {
    it("returns 200", function(done) {
      supertest(app)
        .get("/")
        .expect(200)
        .end(function(err, res){
          if (err) { done(err); } 
          else { done(); }
        });
    });
  });
  describe("GET /thispage/doesnotexist", function() {
    it("returns 404", function(done) {
      supertest(app)
        .get("/thispage/doesnotexist")
        .expect(404)
        .end(function(err, res){
          if (err) { done(err); } 
          else { done(); }
        });
    });
  });
});
