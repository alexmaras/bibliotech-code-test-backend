var superagent = require('superagent');
var oauthSignature = require('oauth-signature');
var agent = superagent.agent();

var ltiConsumerRequest = {
  oauth_nonce: 'bogusnonce',
  oauth_timestep: Date.now(),
  oauth_consumer_key: 'university.of.the.east',
  oauth_signature_method: 'HMAC-SHA1',
  oauth_version: '1.0',
  lti_message_type: 'basic-lti-launch-request',
  lti_version: 'LTI-1p0',
  resource_link_id: '87978',
  resource_link_title: 'Launch the TP',
  resource_link_description: ' Will ET phone home, or not; click to discover more.',
  user_id: '2912',
  roles: 'urn:lti:instrole:ims/lis/Student',
  lis_person_name_full: 'East Student',
  lis_person_name_family: 'Student',
  lis_person_name_given: 'East',
  lis_person_contact_email_primary: 'east.student@east.edu.gov.au',
  lis_person_sourcedid: 'sis:942a8dd9',
  user_image: 'https://ltiapps.net/test/images/lti.gif',
  context_id: '788733',
  context_type: 'CourseSection',
  context_title: 'Bibiotech Code Test',
  context_label: 'BCT101',
  lis_course_offering_sourcedid: 'DD-ST101',
  lis_course_section_sourcedid: 'DD-ST101:C1',
  tool_consumer_info_version: '1.2',
  tool_consumer_instance_guid: 'east.edu.gov.au',
  tool_consumer_instance_name: 'University of the East',
  tool_consumer_instance_description: 'A Higher Education establishment in a land far, far away.',
  tool_consumer_instance_contact_email: 'vle@north.edu.gov.au',
  tool_consumer_instance_url: 'https://vle.uni.ac.uk/',
  launch_presentation_css_url: 'https://ltiapps.net/test/css/tc.css',
  launch_presentation_locale: 'en-GB',
  lis_outcome_service_url: 'https://ltiapps.net/test/tc-outcomes.php',
  lis_result_sourcedid: '195fc402c702d1e44055050786c63a9b:::788733:::29123:::dyJ86SiwwA9',
  ext_ims_lis_basic_outcome_url: 'https://ltiapps.net/test/tc-ext-outcomes.php',
  ext_ims_lis_resultvalue_sourcedids: 'decimal',
  ext_ims_lis_memberships_url: 'https://ltiapps.net/test/tc-ext-memberships.php',
  ext_ims_lis_memberships_id: '195fc402c702d1e44055050786c63a9b:::4jflkkdf9s',
  ext_ims_lti_tool_setting_url: 'https://ltiapps.net/test/tc-ext-setting.php',
  ext_ims_lti_tool_setting_id: '195fc402c702d1e44055050786c63a9b:::d94gjklf954kj',
  custom_tc_profile_url: 'https://ltiapps.net/test/tc-profile.php/195fc402c702d1e44055050786c63a9b',
  custom_system_setting_url: 'https://ltiapps.net/test/tc-settings.php/system/195fc402c702d1e44055050786c63a9b',
  custom_context_setting_url: 'https://ltiapps.net/test/tc-settings.php/context/195fc402c702d1e44055050786c63a9b',
  custom_link_setting_url: 'https://ltiapps.net/test/tc-settings.php/link/195fc402c702d1e44055050786c63a9b',
  custom_lineitems_url: 'https://ltiapps.net/test/tc-outcomes2.php/195fc402c702d1e44055050786c63a9b/788733/lineitems',
  custom_results_url: 'https://ltiapps.net/test/tc-outcomes2.php/195fc402c702d1e44055050786c63a9b/788733/lineitems/dyJ86SiwwA9/results',
  custom_lineitem_url: 'https://ltiapps.net/test/tc-outcomes2.php/195fc402c702d1e44055050786c63a9b/788733/lineitems/dyJ86SiwwA9',
  custom_result_url: 'https://ltiapps.net/test/tc-outcomes2.php/195fc402c702d1e44055050786c63a9b/788733/lineitems/dyJ86SiwwA9/results/29123',
  custom_context_memberships_url: 'https://ltiapps.net/test/tc-memberships.php/context/195fc402c702d1e44055050786c63a9b',
  custom_link_memberships_url: 'https://ltiapps.net/test/tc-memberships.php/link/195fc402c702d1e44055050786c63a9b',
  oauth_callback: 'about:blank'
};

var consumerSecret = 'filledabrasiontwentyovertime';

var signature = oauthSignature.generate('POST', 'http://localhost/users/lti', ltiConsumerRequest, consumerSecret, null, { encodeSignature: false});

exports.login = function (request, done) {
  request
    .post('/users/lti')
    .send(ltiConsumerRequest)
    .end(function (err, res) {
      if (err) {
        throw err;
      }
      agent.saveCookies(res);
      done(agent);
    });
};

