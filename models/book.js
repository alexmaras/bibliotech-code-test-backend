module.exports = function(sequelize, DataTypes, metamodels) {

  const Book = sequelize.define("Book", {
    isbn: {
      type: DataTypes.STRING(13),
      allowNull: false,
      primaryKey: true,
    },
    title: {
      type: DataTypes.STRING(128),
      allowNull: false,
    },
  });

  Book.associate = function(models) {
    this.belongsToMany(models.Institution, {through: models.InstitutionBook, foreignKey: 'book_isbn'})
  };
    
  return Book;
};
