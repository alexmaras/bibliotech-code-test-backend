module.exports = function(sequelize, DataTypes, metamodels) {

  const InstitutionBook = sequelize.define("InstitutionBook", {
    academic_accessible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    student_accessible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  });

  return InstitutionBook;
    
};
