// This script is run as part of `npm run init-db`.
// Please do not change this script - it takes care of populating Books and Institutions with minimal data

const models = require('../models/index.js')('sqlite3.db');

// populate tables
(async () => {
  try {
    await models.Institution.bulkCreate([
      {
        name: 'University of the East'
      },
      {
        name: 'University of the North'
      },
      {
        name: 'University of the South-West'
      },
    ]);

    await models.LTIConsumer.bulkCreate([
      {
        id: 'e05c5979f3f5450a907c3bc09c65ec5f',
        InstitutionName: 'University of the East',
        oauth_consumer_key: 'university.of.the.east',
        oauth_consumer_secret:'filledabrasiontwentyovertime',
      },
      {
        id: '54cd4badbadc4f0f8145a0c8982bf534',
        InstitutionName: 'University of the North',
        oauth_consumer_key: 'university.of.the.north',
        oauth_consumer_secret: 'wholechitchatseriessynopsis',
      },
      {
        id: '9d33564602c144f8b29a3ff520812bd2',
        InstitutionName: 'University of the South-West',
        oauth_consumer_key: 'university.of.the.south-west',
        oauth_consumer_secret: 'catapultmashedtightnessresolved',
      },
    ]);

    await models.Book.bulkCreate([
      {
        isbn: '9781351831215',
        title: 'Organic Solar Cells',
      },
      {
        isbn: '9781136559860',
        title: 'Planning and Installing Photovoltaic Systems',
      },
      {
        isbn: '9781317963974',
        title: 'Solar Cooling',
      },
      {
        isbn: '9781498772785',
        title: 'Photovoltaic Systems Engineering',
      },
      {
        isbn: '9781317671299',
        title: 'Solar, Wind and Land',
      },
      {
        isbn: '9781351831321',
        title: 'Alternative Energy Technologies',
      },
      {
        isbn: '9781482261806',
        title: 'Electricity and Electronics for Renewable Energy Technology',
      },
      {
        isbn: '9781351679725',
        title: 'Underwater Acoustic Modeling and Simulation',
      },
      {
        isbn: '9781136547614',
        title: 'Solar Economy',
      },
      {
        isbn: '9781317308737',
        title: 'Solar Farms',
      },
      {
        isbn: '9781498723305',
        title: 'Organic Solar Cells',
      },
    ]);

    await models.InstitutionBook.bulkCreate([
      // University of the East
      {
        book_isbn: '9781351831215',
        institution_name: 'University of the East',
        student_accessible: true,
        academic_accessible: true,
      },
      {
        book_isbn: '9781317963974',
        institution_name: 'University of the East',
        student_accessible: true,
        academic_accessible: true,
      },
      {
        book_isbn: '9781317671299',
        institution_name: 'University of the East',
        student_accessible: false,
        academic_accessible: true,
      },
      {
        book_isbn: '9781317308737',
        institution_name: 'University of the East',
        student_accessible: false,
        academic_accessible: true,
      },
      {
        book_isbn: '9781498723305',
        institution_name: 'University of the East',
        student_accessible: false,
        academic_accessible: true,
      },
      // University of the South-West
      {
        book_isbn: '9781351831215',
        institution_name: 'University of the South-West',
        student_accessible: true,
        academic_accessible: true,
      },
      {
        book_isbn: '9781136559860',
        institution_name: 'University of the South-West',
        student_accessible: true,
        academic_accessible: true,
      },
      {
        book_isbn: '9781317963974',
        institution_name: 'University of the South-West',
        student_accessible: true,
        academic_accessible: true,
      },
      {
        book_isbn: '9781351831321',
        institution_name: 'University of the South-West',
        student_accessible: true,
        academic_accessible: true,
      },
      {
        book_isbn: '9781351679725',
        institution_name: 'University of the South-West',
        student_accessible: true,
        academic_accessible: true,
      },
      {
        book_isbn: '9781136547614',
        institution_name: 'University of the South-West',
        student_accessible: true,
        academic_accessible: true,
      },
      {
        book_isbn: '9781498723305',
        institution_name: 'University of the South-West',
        student_accessible: true,
        academic_accessible: true,
      },
      // University of the North
      {
        book_isbn: '9781136559860',
        institution_name: 'University of the North',
        student_accessible: true,
        academic_accessible: true,
      },
      {
        book_isbn: '9781317963974',
        institution_name: 'University of the North',
        student_accessible: true,
        academic_accessible: true,
      },
      {
        book_isbn: '9781317671299',
        institution_name: 'University of the North',
        student_accessible: true,
        academic_accessible: true,
      },
      {
        book_isbn: '9781351831321',
        institution_name: 'University of the North',
        student_accessible: true,
        academic_accessible: true,
      },
      {
        book_isbn: '9781482261806',
        institution_name: 'University of the North',
        student_accessible: true,
        academic_accessible: true,
      },
      {
        book_isbn: '9781351679725',
        institution_name: 'University of the North',
        student_accessible: true,
        academic_accessible: true,
      },
    ]);

    process.exit(0)
  } catch (error) {
    console.error(`${error} encountered in /scripts/populate-db.js`);
    process.exit(1)
  }

})();
